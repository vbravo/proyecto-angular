import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-buscar-heroe',
  templateUrl: './buscar-heroe.component.html'

})
export class BuscarHeroeComponent implements OnInit {

  constructor( private activatedRouter: ActivatedRoute ) { }

  ngOnInit(): void {

    this.activatedRouter.params.subscribe( params => {
      console.log(params['termino']);
    });
  }

}
